####爬取大众网热门排行榜店铺基本信息，不包括评论，找到真实的请求，即调用API接口返回的的JSON数据，修改request
import json
import urllib.request
import pandas as pd

####修改此处request网址
request='http://www.dianping.com/mylist/ajax/shoprank?cityId=5&shopType=10&rankType=popscore&categoryId=0 
response=urllib.request.urlopen(request)
obj=json.load(response)
shopname=[]
shopid=[]
category=[]
address=[]
avgprice=[]
region=[]
tscore=[]
escore=[]
sscore=[]
for i in range(len(obj['shopBeans'])):
         shopname.append(obj['shopBeans'][i]['fullName'])
         category.append(obj['shopBeans'][i]['mainCategoryName'])
         avgprice.append(obj['shopBeans'][i]['avgPrice'])
         address.append(obj['shopBeans'][i]['fullAdress'])
         region.append(obj['shopBeans'][i]['mainRegionName'])
         shopid.append(obj['shopBeans'][i]['shopId'])
         tscore.append(obj['shopBeans'][i]['score1'])
         escore.append(obj['shopBeans'][i]['score2'])
         sscore.append(obj['shopBeans'][i]['score3'])
data={'shopid':shopid,'shopname':shopname,'category':category,'address':address,'avgprice':avgprice,'region':region,'tscore':tscore,'escore':escore,'sscore':sscore}
shoprank=pd.DataFrame(data=data,columns=['shopid','shopname','avgprice','category','address','region','tscore','escore','sscore'])
####输出到EXCEL，修改对应的路径
writer = pd.ExcelWriter('E:/NLP/pachong/0811rankshop.xlsx')
shoprank.to_excel(writer,'Sheet1')
writer.save()
