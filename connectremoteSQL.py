###此程序实现将本地数据写入远程服务器的MySQL


# coding: utf-8
import pandas as pd
import pymysql
from sqlalchemy import create_engine
from sshtunnel import SSHTunnelForwarder

hostname='127.0.0.1'
database='pythontest'
username='analyzer'
password='analyzer@tbs2016'
path='E:/NLP/pachong/dpdata.xlsx' ###修改path
data=pd.read_excel(path,encoding='utf-8')
data=pd.DataFrame(data,columns=['comment_id','shopid','shopname','category','address','avgprice','tscore','escore','sscore','userid','created_at','content','usertscore','userescore','usersscore'])
conn=pymysql.connect(host='127.0.0.1',port=3306,user='analyzer',password='analyzer@tbs2016',database='pythontest',charset='utf8mb4')
###修改ssh_key为自己对应id_rsa所在路径
with SSHTunnelForwarder(('192.168.1.163',22),ssh_username='xxwang',ssh_pkey='C:/Users/xiaoxiannv/.ssh/id_rsa',remote_bind_address=('localhost', 3306)) as server:
        engine=create_engine('mysql+pymysql://{}:{}@{}:{}/{}?charset=utf8mb4'.format(username,password,hostname,server.local_bind_port,database),echo=False)
        data.to_sql('dpdata',engine,schema='pythontest',if_exists='append',index=False)###’dpdata‘为远程MySQL中存放该数据的表名，schema为对应的database名
