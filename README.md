基于pyspider的大众点评的爬虫

获取单个店铺评论信息：
    getsingleshopcoomment.py--在pyspider框架下运行，保存JSON形式的结果文件；
    singleshopcommenttoexcel.py--本地python环境下运行，无需pyspider框架，解析结果文件，以二维表格式存入EXCEL表格中；


获取热门排行榜上所有店铺的评论信息：
    getdzdpcomment.py--pyspider下运行，保存JSON形式的结果文件；
    dzdpjsontoexcel.py--本地python环境下运行，无需pyspider框架，解析结果文件，以二维表格式存入EXCEL表格中；
    connectremoteSQL.py--输入为上一布得到的excel文件，直接在python环境下，将数据写入远程服务器的MySQL数据库中；
    

获取排行榜上的商铺信息(不包括具体评论信息)：
    rankshop.py--不需要在pyspider环境下运行，直接在python环境下，找到排行榜直接请求返回的调用JSON的API接口，解析网址，结果存入EXCEL表格中；
    

常见问题及解决方法：
HTTP:500错误--重启pyspider环境或更换登陆账号，改变cookies内容；
HTTP:404错误--在crawl__config中加代理IP,proxy:XXX.XXX.XXXXX

