###解析pyspider的结果文件（json格式），以二维表形式写入excelz中

import json
import re
import pandas as pd
file='E:/NLP/pachong/0810dp.json'###修改此处路径
data=[json.loads(row) for row in open(file,encoding='utf8')]
category=[]
shopname=[]
address=[]
avgprice=[]
shopid=[]
escore=[]###店铺环境平均分
tscore=[]###口味平均分
sscore=[]###服务平均分
userid=[]
comment=[]###评论内容
created_at=[]###评论日期
usertscore=[]###用户口味打分
userescore=[]###用户环境打分
usersscore=[]###用户服务打分
for i in range(len(data)):
    ####爬取的信息中部分字段数据可能为空，比较长度然后为空值赋予NA值
    if len(data[i]['result']['comment']['userid'])>len(data[i]['result']['comment']['s1']):
        for j in range(len(data[i]['result']['comment']['s1'])):
            shopid.append(data[i]['result']['shopid'])
            shopname.append(data[i]['result']['shop-name'])
            category.append(data[i]['result']['cate'])
            avgprice.append(data[i]['result']['avgprice'])
            address.append(data[i]['result']['address'])
            escore.append(data[i]['result']['envirscores'])
            tscore.append(data[i]['result']['foodcores'])
            sscore.append(data[i]['result']['serviscores'])
            userid.append(data[i]['result']['comment']['userid'][j])
            comment.append(data[i]['result']['comment']['text'][j])
            created_at.append(data[i]['result']['comment']['time'][j])
            usertscore.append(data[i]['result']['comment']['s1'][j])
            userescore.append(data[i]['result']['comment']['s2'][j])
            usersscore.append(data[i]['result']['comment']['s3'][j])
        for m in range(len(data[i]['result']['comment']['userid'])-len(data[i]['result']['comment']['s1'])):
            shopid.append(data[i]['result']['shopid'])
            shopname.append(data[i]['result']['shop-name'])
            category.append(data[i]['result']['cate'])
            avgprice.append(data[i]['result']['avgprice'])
            address.append(data[i]['result']['address'])
            escore.append(data[i]['result']['envirscores'])
            tscore.append(data[i]['result']['foodcores'])
            sscore.append(data[i]['result']['serviscores'])
            userid.append(data[i]['result']['comment']['userid'][m])
            comment.append(data[i]['result']['comment']['text'][m])
            created_at.append(data[i]['result']['comment']['time'][m])
            usertscore.append('NA')
            userescore.append('NA')
            usersscore.append('NA')
    elif len(data[i]['result']['comment']['userid'])<len(data[i]['result']['comment']['s1']):
        for j in range(len(data[i]['result']['comment']['userid'])):
            shopid.append(data[i]['result']['shopid'])
            shopname.append(data[i]['result']['shop-name'])
            category.append(data[i]['result']['cate'])
            avgprice.append(data[i]['result']['avgprice'])
            address.append(data[i]['result']['address'])
            escore.append(data[i]['result']['envirscores'])
            tscore.append(data[i]['result']['foodcores'])
            sscore.append(data[i]['result']['serviscores'])
            userid.append(data[i]['result']['comment']['userid'][j])
            comment.append(data[i]['result']['comment']['text'][j])
            created_at.append(data[i]['result']['comment']['time'][j])
            usertscore.append(data[i]['result']['comment']['s1'][j])
            userescore.append(data[i]['result']['comment']['s2'][j])
            usersscore.append(data[i]['result']['comment']['s3'][j])
        for m in range(len(data[i]['result']['comment']['S1'])-len(data[i]['result']['comment']['userid'])):
            shopid.append(data[i]['result']['shopid'])
            shopname.append(data[i]['result']['shop-name'])
            category.append(data[i]['result']['cate'])
            avgprice.append(data[i]['result']['avgprice'])
            address.append(data[i]['result']['address'])
            escore.append(data[i]['result']['envirscores'])
            tscore.append(data[i]['result']['foodcores'])
            sscore.append(data[i]['result']['serviscores'])
            userid.append('NA')
            comment.append('NA')
            created_at.append('NA')
            usertscore.append(data[i]['result']['comment']['s1'][m])
            userescore.append(data[i]['result']['comment']['s2'][m])
            usersscore.append(data[i]['result']['comment']['s3'][m])
    elif len(data[i]['result']['comment']['userid'])==len(data[i]['result']['comment']['s1']):
        for j in range(len(data[i]['result']['comment']['s1'])):
            shopid.append(data[i]['result']['shopid'])
            shopname.append(data[i]['result']['shop-name'])
            category.append(data[i]['result']['cate'])
            avgprice.append(data[i]['result']['avgprice'])
            address.append(data[i]['result']['address'])
            escore.append(data[i]['result']['envirscores'])
            tscore.append(data[i]['result']['foodcores'])
            sscore.append(data[i]['result']['serviscores'])
            userid.append(data[i]['result']['comment']['userid'][j])
            comment.append(data[i]['result']['comment']['text'][j])
            created_at.append(data[i]['result']['comment']['time'][j])
            usertscore.append(data[i]['result']['comment']['s1'][j])
            userescore.append(data[i]['result']['comment']['s2'][j])
            usersscore.append(data[i]['result']['comment']['s3'][j])

####数据处理，提取数字信息
usertscore=[re.sub("\D",'',x)  for x in usertscore]
usersscore=[re.sub("\D",'',x)  for x in usersscore]
userescore=[re.sub("\D",'',x)  for x in userescore]
avgprice=[re.sub("\D",'',x)  for x in avgprice]
escore=[re.findall(r"\d+\.?\d*",x)[0] for x in escore]
tscore=[re.findall(r"\d+\.?\d*",x)[0] for x in tscore]
sscore=[re.findall(r"\d+\.?\d*",x)[0] for x in sscore]
dpdata={'shopid':shopid,'shopname':shopname,'category':category,'address':address,'avgprice':avgprice,'escore':escore,'tscore':tscore,'sscore':sscore,'userid':userid,'created_at':created_at,'comment':comment,'userescore':userescore,'usersscore':usersscore,'usertscore':usertscore}
dpdata=pd.DataFrame(data=dpdata,columns=['shopid','shopname','category','avgprice','address','escore','tscore','sscore','userid','created_at','comment','usertscore','userescore','usersscore'])
writer = pd.ExcelWriter('E:/NLP/pachong/0810dp.xlsx')###改变此处路径
dpdata.to_excel(writer,'Sheet2')
writer.save()

