#####利用pyspider框架获取单个店铺的评论信息，包括用户名/用户ID/用户评论日期/评论内容/用户打分（s1/s2/s3 分别指 用户对口味/环境/服务的打分）

from pyspider.libs.base_handler import *
from bs4 import BeautifulSoup
import re
import math

class Handler(BaseHandler):
    ####修改‘cookies‘为自己账号登陆信息
    
    crawl_config = {
        'headers': {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',	
            'Accept-Language': 'zh-CN,zh;q=0.8',
            'Accept-Encoding':'gzip, deflate',
            'Connection':'keep-alive',
            'Host':'www.dianping.com',
            'Proxy-Connection':'keep-alive',
          },
       'cookies': {
       'JSESSIONID':'195C5E8F4D2B5CC7934AB0DFEC40B228',				
       '__utma':'1.742545750.1499062129.1499062129.1499072114.2',				
       '__utmz':'1.1499062129.1.1.utmcsr=google.co.jp|utmccn=(referral)|utmcmd=referral|utmcct=/',
       '_hc.v':'2a464d24-8cd8-025c-ff33-0e6a10221a85.1499062129',				
       '_lx_utm':'utm_source%3Dgoogle.co.jp%26utm_medium%3Dreferral%26utm_content%3D%252F',				
       '_lxsdk':'15dcc2592467e-03cbd809cdd903-791238-100200-15dcc259247c8',				
       '_lxsdk_cuid':'15dcc2592467e-03cbd809cdd903-791238-100200-15dcc259247c8',			
       '_lxsdk_s':'15dcc25924b-4cf-96f-e3a%7C%7C8',				
       'aburl':'1',		
       'cy':'5',		
       'cye':'nanjing',				
       'dper':'d8c443071b08ea7427f75e96c249d1cf6c77c6a07154c7a7ff283528cefc4fdd',				
       'll':'7fd06e815b796be3df069dec7836c3df',			
       'ua':'dpuser_8415161174'
       
       }
    }

    @every(minutes=24 * 60)
    def on_start(self):
        ####修改此处网址为对应店铺评论的网址
        self.crawl('http://www.dianping.com/shop/68992918/review_more#rev_358248382', callback=self.index_page)

    @config(age=10 * 24 * 60 * 60)
    def index_page(self, response):
        cn=response.doc('.active > em:nth-child(2)').text() ###获取评论总数数据
        comment_count=float(re.sub("\D","",cn))            ###提取获得信息的数字
        maxpage=math.ceil(comment_count/float(20))         ###计算求得评论页数
        for x in range(1,maxpage+1):
            url="https://www.dianping.com/shop/68992918/review_more?pageno="+str(x)   ###修改次数shopid为对应商铺id
            self.crawl(url, callback=self.detail_page)

    @config(priority=2)
    def detail_page(self, response):
        soup=BeautifulSoup(response.text)
        cl=soup.find('div',{'class','comment-list'}).find('ul')
        return{
            'userid':[lis['user-id']  for lis in cl.find_all('a',{'class','J_card'})],
            'user_name':[lis.get_text()  for lis in cl.find_all('p',{'class','name'})],
            'comments':[lis.get_text().strip('\n').strip() for lis in cl.find_all('div',{'class','J_brief-cont'})],
            'created_at':[lis.get_text() for lis in cl.find_all('span',{'class','time'})],
            's1':[lis.find_all('span',{'class','rst'})[0].get_text().strip() for lis in cl.find_all('div',{'class','comment-rst'})],
            's2':[lis.find_all('span',{'class','rst'})[1].get_text().strip() for lis in cl.find_all('div',{'class','comment-rst'})],
            's3':[lis.find_all('span',{'class','rst'})[2].get_text().strip() for lis in cl.find_all('div',{'class','comment-rst'})]
        }