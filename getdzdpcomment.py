#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# Created on 2017-07-11 22:03:02
# Project: dzdpdata20
###获取本周热门排行榜上的店铺评论信息，共100家，建议分批次爬，比如一次20家
###先解析热门排行榜直接请求的JSON网址数据，获得shopid
###用shopid生成网址进入店铺详情页面获取店铺名/店铺类型/店铺地址/店铺价格/店铺评分/人均价格

from pyspider.libs.base_handler import *
from bs4 import BeautifulSoup
import re
import math

class Handler(BaseHandler):
    crawl_config = {
        'headers': {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',	
            'Accept-Language': 'zh-CN,zh;q=0.8',
            'Accept-Encoding':'gzip, deflate',
            'Connection':'keep-alive',
            'Host':'www.dianping.com',
            'Proxy-Connection':'keep-alive',
          },
          
        ###建议修改cookoies内容
       'cookies': {
           'cy':'5',
           'cye':'nanjing',
           '_hc.v':'80ce1a70-7517-e116-3570-b80c438d1a8c.1499235231',
           '__utma':'1.174187498.1499235231.1499235231.1499235231.1',
           'aburl':'1',
           'JSESSIONID':'773AD8DD986FF04FC1FE40A81162CCE2',
           'dper':'b04dbeb1d9f91e1a711db7c693864dbc3a6f326abffa4672f5d77d47d1786a74',
           'll':'7fd06e815b796be3df069dec7836c3df',
           'ua':'dpuser_5906378601',
           'ctu':'08e83e4b12d448d5ec2c9a72777461f1a17f04781a185063c55207eafa8407af',
           'uamo':'15751868578',
       } 
    }
   
    ###进入排行榜直接请求的JSON网页
    @every(minutes=24 * 60)
    def on_start(self):
        self.crawl('http://www.dianping.com/mylist/ajax/shoprank?cityId=5&shopType=10&rankType=popscore&categoryId=0', callback=self.index_page)
    
    ###解析JSON网页，获得shopid,利用shopid生成网址，进入商铺详情页
    @config(age=10 * 24 * 60 * 60)
    def index_page(self, response):
        x=response.json['shopBeans']
        for i in range(0,100):
            shopid=x[i]['shopId']
            url='http://www.dianping.com/shop/'+str(shopid)
            self.crawl(url,callback=self.list_page,auto_recrawl=True,save={'id':shopid})
     
    ###获得商铺基本信息，并进入评论页面      
    def list_page(self,response):
        soup=BeautifulSoup(response.text)
        iid=response.save['id']##shopid
        name=response.doc('#body > div.body-content.clearfix > div.breadcrumb > span').text()##shopname
        cate=response.doc('.breadcrumb > a:nth-child(3)').text()###商铺类型
        addr=response.doc('#basic-info > div.expand-info.address > span.item').text()##商铺地址
        avg=response.doc('#avgPriceTitle').text()##商铺人均消费
        s1=response.doc('#comment_score > span:nth-child(1)').text()###商铺打分：口味平均分
        s2=response.doc('#comment_score > span:nth-child(2)').text()###环境平均分
        s3=response.doc('#comment_score > span:nth-child(3)').text()###服务平均分
        cc=soup.find('span',{'class','item'}).get_text()###包含总评论数的信息
        comment_count=float(re.sub("\D","",cc))
        maxpage=math.ceil(comment_count/float(20))
        url='http://www.dianping.com'+soup.find('p',{'class','comment-all'}).find('a').get('href')
        self.crawl(url,callback=self.com_page,auto_recrawl=True,save={'maxpage':maxpage,'id':iid,'name':name,'cate':cate,'addr':addr,'avg':avg,'s1':s1,'s2':s2,'s3':s3})

     
    ###根据评论页数，依次进入评论页面
    def com_page(self,response):
        iid=response.save['id']
        maxpage=response.save['maxpage']
        for i in range(1,int(maxpage)+1):
            url='http://www.dianping.com/shop/'+str(iid)+'/review_more?pageno='+str(i)
            self.crawl(url,callback=self.detail_page,auto_recrawl=True,save={'id':response.save['id'],'name':response.save['name'],'cate':response.save['cate'],'addr':response.save['addr'],'avg':response.save['avg'],'s1':response.save['s1'],'s2':response.save['s2'],'s3':response.save['s3']})
        

    
    
    ###解析评论页面，获得用户id（userid）/评论日期(time)/内容（text）/用户口味、环境、服务打分（s1,s2,s3）
    @config(priority=2)
    def detail_page(self, response):
        soup=BeautifulSoup(response.text)
        cl=soup.find('div',{'class','comment-list'}).find('ul')
        return {
            'url': response.url,
            'shopid':response.save['id'],
            'shop-name':response.save['name'],
            'address':response.save['addr'],
            'avgprice':response.save['avg'],
            'cate':response.save['cate'],
            'foodcores':response.save['s1'],###口味平均分
            'envirscores':response.save['s2'],###环境平均分
            'serviscores':response.save['s3'],###服务平均分
            'comment':{'userid':[lis['user-id']  for lis in cl.find_all('a',{'class','J_card'})],'time':[lis.get_text() for lis in cl.find_all('span',{'class','time'})],'text':[lis.get_text().strip('\n').strip() for lis in cl.find_all('div',{'class','J_brief-cont'})],'s1':[lis.find_all('span',{'class','rst'})[0].get_text().strip() for lis in cl.find_all('div',{'class','comment-rst'})],'s2':[lis.find_all('span',{'class','rst'})[1].get_text().strip() for lis in cl.find_all('div',{'class','comment-rst'})],'s3':[lis.find_all('span',{'class','rst'})[2].get_text().strip() for lis in cl.find_all('div',{'class','comment-rst'})]}
                       
        }
