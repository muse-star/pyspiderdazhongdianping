####将爬取到的单个店铺的评论信息的json文件解析，得到用户ID/用户名/评论日期/评论内容/用户打分，并输出到excel中

import json
import re
import pandas as pd

####将 file 修改为相应的json文件路径
file='E:/NLP/pachong/0828mingguowangshi.json'
data=[json.loads(row) for row in open(file,encoding='utf8')]
username=[]
userid=[]
comment=[]
created_at=[]
usertscore=[]
userescore=[]
usersscore=[]
for i in range(len(data)):
    if len(data[i]['result']['userid'])>len(data[i]['result']['s1']):
        for j in range(len(data[i]['result']['s1'])):
            username.append(data[i]['result']['user_name'][j])
            userid.append(data[i]['result']['userid'][j])
            comment.append(data[i]['result']['comments'][j])
            created_at.append(data[i]['result']['created_at'][j])
            usertscore.append(data[i]['result']['s1'][j])
            userescore.append(data[i]['result']['s2'][j])
            usersscore.append(data[i]['result']['s3'][j])
        for m in range(len(data[i]['result']['userid'])-len(data[i]['result']['s1'])):
            username.append(data[i]['result']['user_name'][m])
            userid.append(data[i]['result']['userid'][m])
            comment.append(data[i]['result']['comments'][m])
            created_at.append(data[i]['result']['created_at'][m])
            usertscore.append(0)
            userescore.append(0)
            usersscore.append(0)
    if len(data[i]['result']['userid'])<=len(data[i]['result']['s1']):
        for j in range(len(data[i]['result']['userid'])):
            username.append(data[i]['result']['user_name'][j])
            userid.append(data[i]['result']['userid'][j])
            comment.append(data[i]['result']['comments'][j])
            created_at.append(data[i]['result']['created_at'][j])
            usertscore.append(data[i]['result']['s1'][j])
            userescore.append(data[i]['result']['s2'][j])
            usersscore.append(data[i]['result']['s3'][j])
        for m in range(len(data[i]['result']['userid'])-len(data[i]['result']['s1'])):
            username.append(0)
            userid.append(0)
            comment.append(0)
            created_at.append(0)
            usertscore.append(data[i]['result']['s1'][m])
            userescore.append(data[i]['result']['s2'][m])
            usersscore.append(data[i]['result']['s3'][m])
dpdata={'username':username,'userid':userid,'created_at':created_at,'comment':comment,'usertscore':usertscore,'userescore':userescore,'usersscore':usersscore}
dpdata=pd.DataFrame(data=dpdata,columns=['username','userid','created_at','comment','usertscore','userescore','usersscore'])
writer = pd.ExcelWriter('E:/NLP/pachong/0828mingguowangshi.xlsx')
dpdata.to_excel(writer,'Sheet1')
writer.save()